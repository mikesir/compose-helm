{{- define "convert_time_to_seconds" }}
{{- if hasSuffix "m" . -}}
{{- mul (. | replace "m" "") 60}}
{{- else if hasSuffix "s" . -}}
{{- . | replace "s" "" }}
{{- end }}
{{- end }}

{{- define "common_labels" -}}
app.kubernetes.io/name: {{ . }}
{{- end }}

{{- define "certificate_secret_name" -}}
ingress-tls-{{ . }}
{{- end -}}

{{- define "healthcheck_spec" -}}
exec:
  command: 
    {{- slice .test 1 | toYaml | nindent 4 }}
  {{- if .start_period }}
  initialDelaySeconds: {{ template "convert_time_to_seconds" .start_period }}
  {{- end }}
  {{- if .interval }}
  periodSeconds: {{ template "convert_time_to_seconds" .interval }}
  {{- end }}
  {{- if .timeout }}
  timeoutSeconds: {{ template "convert_time_to_seconds" .timeout }}
  {{- end }}
  {{- if .retries }}
  failureThreshold: {{ default 10 .retries }}
  {{- end }}
{{- end }}

{{- define "image_pull_policy_converter" -}}
{{- if eq . "always" -}}
Always
{{- else if eq . "never" -}}
Never
{{- else if eq . "if_not_present" -}}
IfNotPresent
{{- else -}}
{{ required "Unrecognized pull_policy value" $.Values.missing }}
{{- end -}}
{{- end -}}

{{- define "service_type_mapping" -}}
{{- if eq . "ingress" -}}
LoadBalancer
{{- else if eq . "host" -}}
NodePort
{{- else -}}
ClusterIP
{{- end -}}
{{- end -}}