# Docker Compose Helm Chart

This helm chart provides the ability to specify a Docker Compose file for the chart's `values.yaml` and it will be converted into Kubernetes resources.

## Example

Given the following (very simple) Docker Compose file, we can generate equivalent Kubernetes resources.

```yaml
services:
  app:
    image: nginx:alpine
    ports:
      - target: 80
        published: 80
        protocol: TCP
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost"]
      interval: 30s
      retries: 3
    deploy:
      mode: replicated
      replicas: 1
      resources:
        limits:
          cpus: 0.5
          memory: 50M
        reservations:
          cpus: 0.1
          memory: 20M
```

We can generate the resources by running the following:

```
helm template -f docker-compose.yml .
```

## Compose Compatibility

The following chart outlines the supported elements from the Compose spec. Before doing so, it is important to note the following considerations:

- Due to how pod-to-pod communication occurs in Kubernetes, _all_ container ports must be listed in the `ports` definition. If a port isn't defined, it won't be accessible over the pod network (a service needs to exist).
- By default, Kubernetes secrets are used, but can't be created/managed through this Helm chart. 

### Services Configuration


| Key | Supported? | Notes |
|-----|------------|-------|
| `blkio_config` | No | |
| `cpu_count`, `cpu_percent`, `cpu_shares`, `cpu_period`, `cpu_quota`, `cpu_rt_runtime`, `cpu_rt_period` | No | |
| `build` | No | No build support provided. All images must exist in a registry. |
| `cap_add`, `cap_drop` | Yes | |
| `cgroup_parent` | No | |
| `command` | Yes | |
| `configs` | Not yet | |
| `configs: short-syntax` | Not yet | |
| `configs: long-syntax` | Not yet | |
| `container_name` | Yes | |
| `credential_spec` | No | |
| `depends_on` | No | |
| `deploy` | Partial | See [Deploy Support](#deploy-configuration) below |
| `devices` | No | |
| `dns` | No | |
| `dns_opt` | No | |
| `dns_search` | No | |
| `domainname` | Yes | |
| `entrypoint` | Yes | |
| `env_file` | No | |
| `environment` | Partial | Only the map syntax is currently supported |
| `expose` | Not yet | |
| `extends` | No | |
| `external_links` | No | |
| `extra_hosts` | No | |
| `group_add` | Not yet | |
| `healthcheck` | Yes | When defined, it is used for both the `readinessProbe` and `livenessProbe`. All probes will use the `exec` option with the specified command |
| `hostname` | Yes | |
| `image` | Yes | |
| `init` | No | |
| `ipc` | No | |
| `isolation` | No | |
| `labels` | Not yet | |
| `links` | No | |
| `logging` | No | Going to only use the built-in k8s logging |
| `mac_address` | No | |
| `mem_swappiness` | No | |
| `memswap_limit` | No | |
| `network_mode` | No | Going to use built-in k8s networking |
| `networks` | Not yet | |
| `oom_kill_disable` | No | |
| `oom_score_adj` | No | |
| `pid` | Not yet | |
| `pids_limit` | No | |
| `platform` | No | |
| `ports` | Yes | All ports will create `ClusterIP` Services.  |
| `ports: short-syntax` | Yes | Only port declarations are supported (no specific IP addresses). If specifying only a single port, it _must_ be a String (wrapped in quotes). |
| `ports: long-syntax` | Yes | Recommended, as it provides support for ingress. See [Ports to Service type Mapping](#ports-to-service-type-mapping) below |
| `privileged` | Yes | |
| `pull_policy` | Yes | |
| `read_only` | Yes | |
| `restart` | Not yet | |
| `secrets` | Yes | All secrets must be `external` |
| `secrets: short-syntax` | Yes | Will mount secrets to `/run/secrets/<secret-name>` |
| `secrets: long-syntax` | Yes | Provides more configuration options |
| `security_opt` | No | |
| `shm_size` | No | |
| `stop_grace_period` | Yes | Supports time in `#m` and `#s`, but not in combinations (e.g. `1m30s`) |
| `stop_signal` | No | Not supported with k8s |
| `sysctls` | No | |
| `tmpfs` | Not yet | |
| `tty` | No | |
| `ulimits` | No | |
| `user` | Yes | Only supports UIDs, not usernames |
| `volumes` | Not yet | |
| `volumes_from` | No | |
| `working_dir` | Yes | |

### Deploy Configuration

| Key | Supported? | Notes |
|-----|------------|-------|
| `endpoint_mode` | No | |
| `labels` | Not yet | |
| `mode` | Partial | `replicated` will create a Deployment. Soon, `global` will create a DaemonSet |
| `placement` | Not yet | |
| `replicas` | Yes | |
| `resources` | Yes | Minus `devices` |
| `restart_policy` | Not yet | |
| `rollback_config` | Not yet | |
| `update_config` | Not yet | |


## Additional Features and Support

The Compose spec does not provide support for all of the capabilities we would like to provide for the platform. But, through the use of extension fields (those that begin with `x-`), we can add our own features.

### Ports to Service type Mapping

In Kubernetes, all network communication goes through a `Service`, including pod-to-pod. The Compose spec was originally designed for local Docker Compose and Swarm environments, where ports used by container-to-container communications didn't need to be explicitly defined. A proposal has been made to add support to the spec ([follow along here](https://github.com/compose-spec/compose-spec/issues/117)). In the meantime, we are adding support (and defaulting) a third option for `ports.mode`.

```yaml
ports:
  - target: 80
    published: 80
    mode: internal
```

When using the other modes, they will be mapped as follows:

| ports.mode | Service type |
|------------|--------------|
| `internal` | `ClusterIP` (default) |
| `host`     | `NodePort` |
| `ingress`  | `LoadBalancer` |

### AWS IAM Role

The Common Platform provides support to allow your pod to automatically assume a role that is owned and managed in a separate account. Note that this requires additional setup (outside of the scope of this document) and usage of a current AWS SDK. When configured correctly, the SDKs will automatically obtain STS tokens and assume the specified role.

To enable this support, add the following property as part of the service's `deploy` configuration:

| Property | Type | Description | Required? |
|----------|------|-------------|-----------|
| `x-aws-iam-role-arn` | `string` | The AWS ARN for the IAM role you wish your application to assume | Only if you want this feature |

#### Example Usage

```yaml
services:
  app:
    image: nginx:alpine
    deploy:
      x-aws-iam-role-arn: arn:aws:iam::123456789012:role/S3Access
```

### Defining Ingress and TLS Certificates

When providing the configuration for a port, ingress information can also be provided, including expected hostnames and paths. This will then create the appropriate `Ingress` and `Certificate` Kubernetes resources. Note that certificate requests are automatically going to happen at this moment and there is no support (yet) to share a certificate across multiple services that might be using the same hostname.

The following properties are supported under the `x-ingress` key on a port configuration:

| Property | Type | Description | Required? |
|----------|------|-------------|-----------|
| `hosts` | `string[]` | The hostnames that should forward traffic to this port | Yes |
| `cert_issuer` | `string` | The name of the `ClusterIssuer` that will issue the certs | Yes |
| `paths` | `string[]` | The paths that should forward traffic to this port (defaults to `["/"]`) | N 

**Note:** If the `hosts` are defined but no `cert_issuer`, only the appropriate `Ingress` objects will be created.

#### Example Usage 

```yaml
services:
  app:
    image: nginx:alpine
    ports:
      - target: 80
        published: 80
        protocol: TCP
        x-ingress:
          hosts:
            - app.example.com
          paths:
            - /
```

### Vault Secret Support (not supported yet, but spec'ing out)

Since the Common Platform encourages the usage of Vault for secure credential management, we have added support to automatically load secrets from Vault into your application.

When specified, a container is started before the application that will fetch the secrets and place them into a volume that is shared with the main application. Each secret will, by default, be mounted into the application container in a tmpfs directory named `/run/secrets/<secret-name>` (location can be changed using the long-syntax for service secret config). The contents of the directory will depend on the specified `file_format`.

The Vault config for a secret is specified using the `x-vault` extension field and supports the following options:

| Property | Type | Description | Required? |
|----------|------|-------------|-----------|
| `path` | `string` | The path in Vault that the secret should be fetched from | Yes |
| `role` | `string` | The name of the Vault role to use during authentication | Yes |
| `type` | `string` | The type of secret (`kv`). Defaults to `kv` | No |
| `file_format` | `string` | How the secret is extracted from Vault into files. See [Supported File Formats](#supported-file-formats) below. Default to `as-is`. | No |

#### Vault Authentication

The Common Platform is leveraging the [Kubernetes auth method](https://www.vaultproject.io/docs/auth/kubernetes) when authenticating with Vault. This leverages the `ServiceAccount` granted to each app to authenticate as a specific role in Vault. That role then has a policy that lets it read the secrets.

In order for a service named `app` to access a Vault secret at `secrets/my-app/db-credentials`, the following will need to be configured (which will typically be done by the Vault admin team):

- A Vault policy will be created that grants access to the secret. Assume that policy is named `my-app-db-credentials-ro`.

- A Vault role will need to be created. The namespace is the tenant name. The service account uses the pattern `deployment-<service-name>`:

    ```cli
    vault write auth/kubernetes/role/my-app-db-credentials-ro \
      bound_service_account_names=deployment-app \
      bound_service_account_namespaces=hello-world-app \
      policies=my-app-db-credentials-ro \
      ttl=1h
    ```

    In this example, the role being created is named `my-app-db-credentials-ro`, which will be plugged into the compose file's secret configuration.

#### Automatic Base64 Decoding

To load binary and multi-line files into Vault, those values are first base64-encoded. To help prevent the need to have additional startup scripts/config that will need to base64 decode, all Vault secret values that begin with `base64:` will be auto-decoded when written into values. 

#### Supported File Formats

Vault secrets are _typically_ stored as key/value pairs encoded in JSON. To support various use cases, those secrets can be saved as files in a variety of ways. The examples below will use a fictitional secret named `db-credentials` in the Compose file:

```json
{
  "username": "username-abcd",
  "password": "SuperSecretPa$$w0rd!",
  "host": "base64:bG9jYWxob3N0"
}
```

- `as-is` - the secret is saved as a single file named `data` with the exact contents of the secret.

    **/run/secrets/db-credentials/data**
    ```json
    {
      "username": "username-abcd",
      "password": "SuperSecretPa$$w0rd!",
      "host": "localhost"
    }
    ```

- `split-files` - the secret is split into many files with each key being its own file. For the example above, we would have the following:

    **/run/secrets/db-credentials/username**
    ```plaintext
    username-abcd
    ```

    **/run/secrets/db-credentials/password**
    ```plaintext
    SuperSecretPa$$w0rd!
    ```

    **/run/secrets/db-credentials/host**
    ```plaintext
    localhost
    ```

- `properties` - the secret is converted from JSON into a Java properties format in a file named `data`.

    **/run/secrets/db-credentials/data**
    ```properties
    username: username-abcd
    password: SuperSecretPa$$w0rd!
    host: localhost
    ```

#### Example Usage

```yaml
services:
  app:
    image: my-app
    secrets:
      - db-credentials
secrets:
  db-credentials:
    external: true
    x-vault:
      path: /secrets/my-app/db-credentials
      role: my-app-db-credentials-ro
      file_format: as-is
```
